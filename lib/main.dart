import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Form Validation Demo';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: MyCustomForm(),
      ),
    );
  }
}
var response;
String printData;

Future<Weather> fetchWeather(var value) async {

  response = await http.get('http://192.168.8.106:1323?cityName='+value);

 print(response.body);

  Map<String, dynamic> json_fetch=json.decode(response.body);

  printData ="Temperature : ${json_fetch['main']['temp']}\n"
      "Pressure : ${json_fetch['main']['pressure']}\n"
      "Humidity: ${json_fetch['main']['humidity']}\n";

  print(printData);


}

class Weather {
  final String desc;
  final double temp;
  final double pressure;
  final double humidity;

  Weather({this.desc,this.temp, this.pressure, this.humidity});

  factory Weather.fromJson(Map<String, dynamic> json) {
    String msg ="Temperature : ${json['main']['temp']}k\n"
    "Pressure : ${json['main']['pressure']}k\n"
    "Humidity: ${json['main']['humidity']}k\n";

    return Weather(
      desc:json['weather']['description'],
      temp: json['main']['temp'],
      pressure: json['main']['pressure'],
      humidity: json['main']['humidity'],
    );
  }
}


// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  Future<Weather> futureWeather;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }else{
                futureWeather = fetchWeather(value);
                return printData;
              }
              return null;
            },
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text('processing')));
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}